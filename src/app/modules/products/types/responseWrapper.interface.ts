export interface IresponseWrapper<T> {
    status: string;
    result: T;
}
