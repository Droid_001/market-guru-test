import { NgModule } from '@angular/core';
import { ListComponent } from './components/list/list.component';
import { ProductsService } from './services/products.service';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { CommonModule } from '@angular/common';
@NgModule({
    declarations: [ListComponent],
    providers: [ProductsService],
    imports: [NzTableModule, NzDividerModule, CommonModule],
})
export class ProductsModule {}
