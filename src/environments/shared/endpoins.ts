export interface Iservice {
    domain: string;
    secure: boolean;
    routes: { [key: string]: string };
}

export interface IserviceList {
    [key: string]: Iservice;
}

export const services = {
    main: {
        domain: `localhost:8080`,
        secure: false,
        routes: {
            products: 'products',
        },
    },
};
