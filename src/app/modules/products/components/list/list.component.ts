import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import { Iproduct } from '../../types/product.interface';
import {
    NzTableFilterFn,
    NzTableFilterList,
    NzTableSortFn,
    NzTableSortOrder,
} from 'ng-zorro-antd/table';

interface IdataItem extends Partial<Iproduct> {
    id: string;
    name: string;
    brandName: string;
    brandId: string;
    availability: number;
}

interface IcolumnItem {
    name: string;
    sortOrder: NzTableSortOrder | null;
    sortFn: NzTableSortFn<IdataItem> | null;
    listOfFilter: NzTableFilterList;
    filterFn: NzTableFilterFn<IdataItem> | null;
    filterMultiple: boolean;
    sortDirections: NzTableSortOrder[];
}

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.less'],
})
export class ListComponent implements OnInit {
    constructor(
        private products: ProductsService,
        private router: Router,
        private route: ActivatedRoute,
    ) {}
    isLoading = false;
    pageIndex = 1;

    listOfColumns: IcolumnItem[] = [];
    productsList: Iproduct[] = [];

    initColumns() {
        this.listOfColumns = [
            {
                name: 'id',
                sortOrder: null,
                sortFn: (a: IdataItem, b: IdataItem) =>
                    a.id.localeCompare(b.name),
                sortDirections: ['ascend', 'descend', null],
                filterMultiple: true,
                listOfFilter: [],
                filterFn: (list: string[], item: IdataItem) =>
                    list.some(id => item.id.indexOf(id) !== -1),
            },
            {
                name: 'name',
                sortOrder: 'descend',
                sortFn: (a: IdataItem, b: IdataItem) =>
                    a.name.localeCompare(b.name),
                sortDirections: ['descend', null],
                listOfFilter: this.productsList.map(elem => ({
                    text: elem.name,
                    value: elem.name,
                })),
                filterFn: (name: string, item: IdataItem) =>
                    item.name.indexOf(name) !== -1,
                filterMultiple: true,
            },
            {
                name: 'brand name',
                sortOrder: null,
                sortDirections: ['ascend', 'descend', null],
                sortFn: (a: IdataItem, b: IdataItem) =>
                    a.brandName.localeCompare(b.brandName),
                filterMultiple: false,
                listOfFilter: this.productsList.map(elem => ({
                    text: elem.brandName,
                    value: elem.brandName,
                })),
                filterFn: (brandName: string, item: IdataItem) =>
                    item.brandName.indexOf(brandName) !== -1,
            },
            {
                name: 'brand id',
                sortOrder: null,
                sortDirections: ['ascend', 'descend', null],
                sortFn: (a: IdataItem, b: IdataItem) =>
                    a.brandId.length - b.brandId.length,
                filterMultiple: false,
                listOfFilter: [],
                filterFn: null,
            },
            {
                name: 'aviability',
                sortOrder: null,
                sortDirections: ['ascend', 'descend', null],
                sortFn: (a: IdataItem, b: IdataItem) =>
                    a.availability - b.availability,
                filterMultiple: false,
                listOfFilter: [],
                filterFn: null,
            },
        ];
    }

    onPageChanged(page: number) {
        this.router.navigate(['.'], {
            relativeTo: this.route,
            queryParams: { page: page === 1 ? null : page },
        });
    }

    ngOnInit(): void {
        this.pageIndex = this.route.snapshot.queryParams['page'] || 1;

        this.isLoading = true;
        this.products.getProducts().subscribe({
            next: data => {
                this.productsList = data;
                this.initColumns();
            },
            complete: () => (this.isLoading = false),
        });
    }
}
