import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { throwError } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';

import { EndpointsService } from 'src/app/endpoints.service';
import { Iproduct } from '../types/product.interface';
import { IresponseWrapper } from '../types/responseWrapper.interface';

@Injectable()
export class ProductsService {
    constructor(
        private http: HttpClient,
        private endpoints: EndpointsService,
    ) {}

    private handleError(error: HttpErrorResponse) {
        console.error(
            `Backend returned  ${
                error?.status ? `code ${error.status}` : ''
            }, body was: `,
            error.error,
        );

        return throwError(
            () => new Error('Something bad happened; please try again later.'),
        );
    }

    getProducts() {
        return this.http
            .get<IresponseWrapper<Iproduct[]>>(
                this.endpoints.get('main', 'products'),
            )
            .pipe(
                retry(3),
                catchError(this.handleError),
                map(response => response.result),
            );
    }
}
