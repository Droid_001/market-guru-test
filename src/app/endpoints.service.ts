import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { replace } from 'lodash';
import { IserviceList } from '../environments/shared/endpoins';

@Injectable({
    providedIn: 'root',
})
export class EndpointsService {
    constructor() {}
    private services: IserviceList = environment.services;

    private paramsParser = (
        inputString: string,
        params: { [key: string]: string | number },
    ) => {
        Object.entries(params).forEach(([key, value]) => {
            inputString = replace(inputString, `%${key}%`, String(value));
        });
        return inputString;
    };

    get = (
        service: keyof IserviceList,
        route: string,
        params?: { [key: string]: string | number },
        query?: { [key: string]: string | number },
    ): string => {
        const currentService = this.services[service];
        if (!currentService) {
            console.error(service + ': no such service');
            return '';
        }
        let p = params
            ? this.paramsParser(currentService.routes[route], params)
            : currentService.routes[route];
        let q = '';
        if (query)
            Object.entries(query).forEach(([key, value]) => {
                q += `${q.length ? '&' : '?'}${key}=${value}`;
            });

        return `http${currentService.secure ? 's' : ''}://${
            currentService.domain
        }/${p}${q}`;
    };
}
